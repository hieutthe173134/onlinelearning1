<%-- 
    Document   : detail
    Created on : Sep 21, 2023, 11:32:33 AM
    Author     : asus
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>...</title>
    </head>
    <body>
        <div>
            
                <div>
                    <a href="detail?cId=${de.cId}">
                        <div>
                            <img src="${de.img}"/>
                            <div>${de.numberStudent}</div>
                            <h5>${de.cName}</h5>
                            <p>${de.detail}</p>
                        </div>
                    </a>
                </div>
            
        </div>
    </body>
</html>
