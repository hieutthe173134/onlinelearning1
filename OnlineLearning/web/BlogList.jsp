<%-- 
    Document   : BlogList
    Created on : Sep 12, 2023, 11:22:39 PM
    Author     : may tinh cua hieu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/blogStyle.css"/>
    </head>
    <body>
<!--        <div>
            <%@include file="header.jsp" %>
        </div>-->
        <div class="content">
            <div class="container"> 
                <div class="search">
                    <form action="bloglist" method="post">
                        <input id ="input-text" name="text" type="text" placeholder="Tìm kiếm"/>
                        <input id = "search-button" type="submit" value="Tìm kiếm"/>
                    </form>
                </div>
                <div class="list-item">
                    <c:forEach items="${b.blog}" var="b">
                        <div class="item">
                            <div class="col-md-4 blog-image">
                                <a href="blogs/${b.blogDetail}" class="img">
                                    <img src="img/${b.img}"/>
                                </a>
                            </div>
                            <div class="col-md-8 blog-detail">
                                <div class="top">
                                    <a href="blogs/${b.blogDetail}"><h3>${b.blogName}</h3></a>
                                    <p>abcd</p>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
<!--        <div>
            <%@include file="footer.jsp" %>
        </div>-->
    </body>
</html>
