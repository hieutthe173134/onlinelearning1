<%-- 
    Document   : register
    Created on : Sep 16, 2023, 3:05:27 PM
    Author     : asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Html.html to edit this template
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style_2.css"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="close"><a href="home.jsp">ｘ</a></div>
            <div class="form-box register">
                <h2 style="margin-top: 30px;">Registration</h2>
                <form action="#">
                    <div class="input-box">
                        <input type="text" required>
                        <label>Username</label>
                    </div>
                    <div class="input-box">
                        <input type="email" required>
                        <label>Email</label>
                    </div>
                    <div class="input-box">
                        <input type="password" required>
                        <label>Password</label>
                    </div>
                    <div class="remember-forgot">
                        <label><input type="checkbox">I agree to the terms & conditions</label>
                    </div>
                    <button type="submit" class="btn">Register</button>
                    <div class="have_register">
                        <p>Already have an account? <a href="login.jsp" class="login-link">Login</a></p>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>

