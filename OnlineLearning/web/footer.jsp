<%-- 
    Document   : footer
    Created on : Sep 21, 2023, 4:38:23 PM
    Author     : may tinh cua hieu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/footer.css"/>
    </head>
    <body>
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-col">
                        <h4></h4>
                        <ul>
                            <li><a href="#">Do code</a></li>
                        </ul>
                    </div>
                    <div class="footer-col">
                        <h4>Liên kết</h4>
                        <ul>
                            <li><a href="#">Học tập</a></li>
                            <li><a href="#">Luyện tập</a></li>
                            <li><a href="#"></a></li>

                        </ul>
                    </div>
                    <div class="footer-col">
                        <h4>Thông tin</h4>
                        <ul>
                            <li><a href="#">chia sẻ</a></li>
                            <li><a href="#">Về chúng tôi</a></li>
                            <li><a href="#">Hỗ trợ</a></li>

                        </ul>
                    </div>
                    <div class="footer-col">
                        <h4>Follow us</h4>
                        <div class="social-links">
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                            <a href="#"><i class="fab fa-youtube"></i></a>

                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
