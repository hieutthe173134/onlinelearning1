/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Blog;

/**
 *
 * @author may tinh cua hieu
 */
public class BlogDAO extends DBContext {

    public List<Blog> getBlog() {
        List<Blog> blogs = new ArrayList<>();
        String sql = "select * from blog";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                blogs.add(new Blog(rs.getString("blogId"), rs.getString("blogName"), rs.getString("img"), rs.getString("author"), rs.getString("dateRelease"), rs.getString("blogDetail")));
            }
        } catch (SQLException e) {

        }
        return blogs;
    }

    public List<Blog> getBlogByText(String text) {
        List<Blog> blogs = new ArrayList<>();
        String sql = "select * from blog where blogName like '%" + text + "%'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                blogs.add(new Blog(rs.getString("blogId"), rs.getString("blogName"), rs.getString("img"), rs.getString("author"), rs.getString("dateRelease"), rs.getString("blogDetail")));

            }
        } catch (SQLException e) {

        }
        return blogs;
    }

    public static void main(String[] args) {
        BlogDAO d = new BlogDAO();
        List<Blog> list = d.getBlog();
        for (Blog blog : list) {
            System.out.println(blog.toString() + blog.getBlogDetail());
        }
    }

}
