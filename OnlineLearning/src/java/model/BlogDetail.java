/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author may tinh cua hieu
 */
public class BlogDetail {
    private String blogId, blogDetail;

    public BlogDetail() {
    }

    public BlogDetail(String blogId, String blogDetail) {
        this.blogId = blogId;
        this.blogDetail = blogDetail;
    }

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getBlogDetail() {
        return blogDetail;
    }

    public void setBlogDetail(String blogDetail) {
        this.blogDetail = blogDetail;
    }
    
     
}
