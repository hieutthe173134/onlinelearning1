/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class Account {
    private int aid;
    private String username;
    private String password;
    private int isSell;
    private int isAdmin;
    private AccountDetail accountdetail;
    public Account() {
    }

    public Account(int aid, String username, String password, int isSell, int isAdmin) {
        this.aid = aid;
        this.username = username;
        this.password = password;
        this.isSell = isSell;
        this.isAdmin = isAdmin;
    }

    public Account(int aid, String username, String password, int isSell, int isAdmin, AccountDetail accountdetail) {
        this.aid = aid;
        this.username = username;
        this.password = password;
        this.isSell = isSell;
        this.isAdmin = isAdmin;
        this.accountdetail = accountdetail;
    }

    public AccountDetail getAccountdetail() {
        return accountdetail;
    }

    public void setAccountdetail(AccountDetail accountdetail) {
        this.accountdetail = accountdetail;
    }
    

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIsSell() {
        return isSell;
    }

    public void setIsSell(int isSell) {
        this.isSell = isSell;
    }

    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public String toString() {
        return "Account{" + "aid=" + aid + ", username=" + username + ", password=" + password + ", isSell=" + isSell + ", isAdmin=" + isAdmin + '}';
    }
    
}
