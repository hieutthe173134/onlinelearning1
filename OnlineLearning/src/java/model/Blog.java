/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author may tinh cua hieu
 */
public class Blog {
    private String blogId, blogName, img, author, dateRelease, blogDetail;

    public Blog() {
    }

    public Blog(String blogId, String blogName, String img, String author, String dateRelease, String blogDetail) {
        this.blogId = blogId;
        this.blogName = blogName;
        this.img = img;
        this.author = author;
        this.dateRelease = dateRelease;
        this.blogDetail = blogDetail;
    }

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDateRelease() {
        return dateRelease;
    }

    public void setDateRelease(String dateRelease) {
        this.dateRelease = dateRelease;
    }

    public String getBlogDetail() {
        return blogDetail;
    }

    public void setBlogDetail(String blogDetail) {
        this.blogDetail = blogDetail;
    }
    
    
    @Override
    public String toString() {
        return blogId + " " + blogName + " " + img;
    }
    
    
}
