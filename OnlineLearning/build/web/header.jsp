<%-- 
    Document   : header
    Created on : Sep 11, 2023, 4:24:10 PM
    Author     : asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login and Regester</title>
    <link rel="stylesheet" href="css/header.css">
</head>
<body>
    <header>
        <h2 class="logo">Logo</h2>
        <nav class="navigation">
            <a href="home.jsp">Home</a>
            <a href="about.jsp">About</a>
            <a href="listAll">Courses</a>
            <a href="contact.jsp">Contact</a>
            <a class="button" href="login.jsp">Login</a>
        </nav>
    </header>

    <script src="css/script.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
</body>
</html>
